<?php get_header(); ?>
<section class="slider">
	<div class="container">
		<div class="row">
			<div class="courses_slider_block clearfix">
				<?php  
				$title_courses_post = get_field( "title_courses_post" ); 
				$prices_courses = get_field( "prices_courses" ); 
				$date_published_post = get_field( "date_published_post" ); 
				$content_courses = get_field( "content_courses" ); 

				$args_recent_courses = array(
					'post_type' => 'course',
					'post_status' => 'publish',
					'post__in'  => get_option( 'sticky_posts' ),
					'ignore_sticky_posts' => 1
					);
				$recent_courses = new WP_Query($args_recent_courses);
				?>
				<?php if($recent_courses->have_posts()) : ?>
					<?php while ($recent_courses->have_posts()) : $recent_courses->the_post(); ?> 

						<div class="archive_content_block">
							<div class="col-lg-4">
								<div class="flexslider">
									<ul class="slides">
										<li class="courses_slider_content">
											<a href="<?php the_permalink(); ?>">
												<?php 
												$images_courses_post = get_field( "images_courses_post" ); 
												if( !empty($images_courses_post) ): ?>
												<img src="<?php echo $images_courses_post['url']; ?>" />
											</a>
										<?php endif; ?>
										<h3><a href="<?php the_permalink(); ?>"><?php the_field('title_courses_post'); ?></a></h3>
										<p><?php the_field('content_courses'); ?></p>
										<div class="courses_slider_bottom">
											<span class="price"><?php the_field('prices_courses') ?></span>
											<div class="courses_slider_date">
												<i class="fa fa-clock-o icon-time"></i>
												<span class="date"><?php the_time('j F Y'); ?></span>
											</div>
										</div>
									</li>
								</div>
							</ul>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>