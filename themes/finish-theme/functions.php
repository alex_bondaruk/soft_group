<?php
/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain('sg', get_template_directory() . '/languages');


/**
 * Додаємо favicon, charset, viewport
 */
function add_head_meta_tags()
{
  ?>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favicon.png" type="image/x-icon">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <?php
}
/**
 * Підключаємо javascript файли
 */
function add_theme_scripts(){
  wp_enqueue_script("jquery");
  wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
  wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array('jquery', 'bootstrap') );
  wp_enqueue_script( 'swiper', get_template_directory_uri() . '/libs/Swiper-3.4.2/dist/js/swiper.min.js', array('jquery', 'bootstrap') );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

/**
 * Підключаємо css файли
 */
function font_awesome() {
  if (!is_admin()) {
    wp_register_style('font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css');
    wp_enqueue_style('font-awesome');
  }
}
add_action('wp_enqueue_scripts', 'font_awesome');

function add_theme_style(){
  wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
  wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
  wp_enqueue_style( 'font', get_template_directory_uri() . '/css/font.css');
  wp_enqueue_style( 'swiper', get_template_directory_uri() . '/libs/Swiper-3.4.2/dist/css/swiper.min.css');
}
add_action( 'wp_enqueue_scripts', 'add_theme_style' );



//add_action('wp_head', 'add_head_meta_tags');


/**
 * Реєструємо місце для меню
 */
register_nav_menus(array(
    'top'    => __('Главное меню'),    //Название месторасположения меню в шаблоне
    ));


/**
 * Дозволяє виводити шорткоди в текстовому віджеті (дефолтному)
 */
add_filter('widget_text', 'do_shortcode');
/**
 * Реєструємо сайдбари теми
 */
if ( function_exists('register_sidebar') )
{
 register_sidebars(1, array(
  'id' => 'left_top_header',
  'name' => __('Left top header', 'sg'),
  'description' => 'Место под телефон, e-mail',
  'before_widget' => '<div class="header_contacts">',
  'after_widget' => '</div>',
  'before_title' => '<h3>',
  'after_title' => '</h3>'
  ));
 register_sidebars(1, array(
  'id' => 'right_top_header',
  'name' => __('Right top header', 'sg'),
  'description' => 'Место под социальные иконки и авторизацию',
  'before_widget' => '<div class="header_socials">',
  'after_widget' => '</div>',
  'before_title' => '<h3>',
  'after_title' => '</h3>'
  ));
 register_sidebars(1, array(
  'id' => 'left_sidebar_new',
  'name' => __('Sidebar Left Block', 'sg'),
  'description' => 'Левый сайдбар с новыми записями в контенте',
  'before_widget' => '<div class="recent_post">',
  'after_widget' => '</div>',
  'before_title' => '<h3>',
  'after_title' => '</h3>'
  ));
 register_sidebars(1, array(
  'id' => 'center_sidebar_new',
  'name' => __('Sidebar Center Block', 'sg'),
  'description' => 'Центральный блок под видео',
  ));
 register_sidebars(1, array(
  'id' => 'left',
  'name' => __('Left sidebar', 'sg'),
  'description' => 'Левый сайдбар в футере',
  'before_widget' => '<div class="recent_post">',
  'after_widget' => '</div>',
  'before_title' => '<h3>',
  'after_title' => '</h3>'
  ));
 register_sidebars(1, array(
  'id' => 'after_slider_block',
  'name' => __('After slider block', 'sg'),
  'description' => 'Место под блок после слайдера',
  'before_widget' => '<div>',
  'after_widget' => '</div>',
  ));
 register_sidebars(1, array(
  'id' => 'twitter',
  'name' => __('Twitter block', 'sg'),
  'description' => 'Место под твиты в блоке'
  ));
 register_sidebars(1, array(
  'id' => 'footer',
  'name' => __('Right_footer_posts', 'sg'),
  'description' => 'Место под последние записи (список) в правом сайдбаре в футере',
  'before_widget' => '<div class="recent_post_list_block">',
  'after_widget' => '</div>',
  'before_title' => '<h3>',
  'after_title' => '</h3>'
  ));
 register_sidebars(1, array(
  'id' => 'footer',
  'name' => __('about_footer', 'sg'),
  'description' => 'Место под блок "About us"',
  'before_widget' => '<div class="about_footer">',
  'after_widget' => '</div>',
  'before_title' => '<p>',
  'after_title' => '</p>'
  ));
 register_sidebars(1, array(
  'id' => 'tag_cloud',
  'name' => __('tag_cloud', 'sg'),
  'description' => 'Место под облако тегов (можно использовать дефолтный)',
  'before_widget' => '<div class="tag_cloud_block">',
  'after_widget' => '</div>',
  'before_title' => '<p>',
  'after_title' => '</p>'
  ));
 register_sidebars(1, array(
  'id' => 'footer',
  'name' => __('Footer_left', 'sg'),
  'description' => 'Нижний левый блок футера',
  'before_widget' => '<div class="footer_bottom_left">',
  'after_widget' => '</div>',
  'before_title' => '<p>',
  'after_title' => '</p>'
  ));
 register_sidebars(1, array(
  'id' => 'footer',
  'name' => __('Footer_right', 'sg'),
  'description' => 'Нижний правый блок футера',
  'before_widget' => '<div class="footer_bottom_right">',
  'after_widget' => '</div>',
  'before_title' => '<p>',
  'after_title' => '</p>'
  ));
 register_sidebars(1, array(
  'id' => 'contact_sidebar',
  'name' => __('Contact sidebar', 'sg'),
  'description' => 'правый блок на странице contacts',
  'before_widget' => '<div class="contact_right">',
  'after_widget' => '</div>',
  'before_title' => '<h3 class="sidebar_right_title">',
  'after_title' => '</h3>'
  ));
}


/**
 * Підключаємо підтримку мініатюр
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
//		set_post_thumbnail_size( 900, 300 );
}
/**
// * Можемо добавити різні розміри для картинок
 */
if ( function_exists( 'add_image_size' ) ) { 
  add_image_size( 'new_post_img', 70, 70, true );
  add_image_size( 'team_post_img', 115, 115, true );
}

/**
 * Реєструємо формати постів
 */
function add_post_formats(){
  add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );
  add_theme_support( 'post-thumbnails' );

  add_theme_support( 'custom-background' );
  add_theme_support( 'custom-header' );
  add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'add_post_formats', 11 );


/**
 * Удаляємо стандартне закінчення обрізаного тексту з [...] 
 */
function custom_excerpt_more( $more ) {
	return '';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );


/**
 * Замінюємо стандартну довжину обрізаного тексту
 */
function custom_excerpt_length( $length ) {
	return 10;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


/* Widget for after slider block*/
add_action('widgets_init', 'after_slider_widget');

function after_slider_widget() {
  register_widget('After_slider_widget');
}


class After_slider_widget extends WP_Widget {
  function __construct () {
    parent::__construct (
            'after_slider_widget',//ID виджета
            esc_html__('After slider widget', 'sg'), //Имя виджета
            array('description'=> esc_html__('Выводит блок информации для блока после слайдера', 'sg'), )
        );//Описание
  }

  public function form ($instance) {
    $img_after_slider = '';
    $title = '';
    $text_after_slider = '';
    ?>
    <p>
      <label for="<?php echo $this->get_field_id('img_after_slider'); ?>"><?= __('Картинка записи:', 'sg'); ?></label>
      <input type="text" name="<?php echo $this->get_field_name('img_after_slider'); ?>" id="<?php echo $this->get_field_id('img_after_slider'); ?>" value="<?= esc_attr($img_after_slider) ?>" class="widefat">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>"><?= __('Title:', 'sg'); ?></label>
      <input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" value="<?= esc_attr($title) ?>" class="widefat">
    </p>    

    <p>
      <label for="<?php echo $this->get_field_id('text_after_slider'); ?>"><?= __('Текст блока after slider:', 'sg'); ?></label>
      <textarea name="<?php echo $this->get_field_name('text_after_slider'); ?>" id="<?php echo $this->get_field_id('text_after_slider'); ?>" value="<?= esc_attr($text_after_slider) ?>" class="widefat"></textarea>
    </p>
    <?php
  }

  public function widget ($args, $instance) { 

    ?>
    <?= $args['before_widget'] ?>
    <div class="col-lg-3">
      <div class="after_slider_blocks">
        <div class="after_slider_img">
          <img src="<?= $instance['img_after_slider'] ?>" alt="">
        </div>
        <div class="after_slider_text">
          <h3><?= $instance['title'] ?></h3>
          <p><?= $instance['text_after_slider'] ?></p>
        </div>
      </div>
    </div>

    <?= $args['after_widget'] ?>
    <?php
  }
  public function update ($newInstance, $oldInstance) {
    $update_after_slider = array();
    $update_after_slider["img_after_slider"] = htmlentities($newInstance["img_after_slider"]);
    $update_after_slider["title"] = htmlentities($newInstance["title"]);
    $update_after_slider["text_after_slider"] = htmlentities($newInstance["text_after_slider"]);
    return $update_after_slider;
  }

}



/* СТВОРЕННЯ ВІДЖЕТА Категорій*/
add_action('widgets_init', 'category_widg');

function category_widg() {
 register_widget('Category_Tax');
}


class Category_Tax extends WP_Widget {
 function __construct () {
  parent::__construct (
			'category_tax',//ID виджета
			esc_html__('CategoryTax', 'sg'), //Имя виджета
			array('description'=> esc_html__('Выводит категории в сайдбар', 'sg'), )
		);//Описание
}

public function form ($instance) {
  $title = '';
  $category = null;
  $limit = 16;

  ?>
  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>"><?= __('Title:', 'sg'); ?></label>
    <input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" value="<?= $title ?>" class="widefat">
  </p>	

  <p>
    <select class="widefat" name="<?php echo $this->get_field_name('category'); ?>" id="<?php echo $this->get_field_id('category'); ?>">
     <?php foreach (get_taxonomies(['show_ui' => true, 'show_in_nav_menus' => true, '_builtin' => true], 'object') as $tax){ ?>
     <option value="<?= $tax->name ?>"<?php if($category == $tax->name){ ?> selected="selected"<?php } ?>><?= $tax->label ?></option>
     <?php } ?>
   </select>
 </p>

 <p>
  <label for="<?php echo $this->get_field_id('limit'); ?>"><?= __('Limit:', 'sg'); ?></label>
  <input type="number" name="<?php echo $this->get_field_name('limit'); ?>" id="<?php echo $this->get_field_id('limit'); ?>" value="<?= $limit ?>" class="widefat">
</p>
<?php
}

public function widget ($args, $instance) {	

  $terms = get_terms([
    'taxonomy' => [ $instance['category'] ],
    'number' => $instance['limit']
    ]);
  $termsLists = [
  0 => [],
  1 => []
  ];
  foreach ($terms as $key => $term){
    if( $key % 2 == 0 ){
      $termsLists[0][] = $term;
    } else {
      $termsLists[1][] = $term;
    }
  }
  ?>
  <?= $args['before_widget'] ?>
  <?= $args['before_title'] ?><?= $instance['title'] ?><?= $args['after_title'] ?>
  <div class="row">
    <div class="col-lg-6">
      <ul class="list-unstyled">
        <?php foreach ($termsLists[1] as $term){
          echo $this->get_tax_item( $term );                          
        } ?>
      </ul>
    </div>
    <!-- /.col-lg-6 -->
    <div class="col-lg-6">
      <ul class="list-unstyled">
        <?php foreach ($termsLists[0] as $term){
          echo $this->get_tax_item( $term );
        } ?>
      </ul>
    </div>
    <!-- /.col-lg-6 -->
  </div>

  <?= $args['after_widget'] ?>
  <?php
}
public function get_tax_item( $term )
{
 return '<li><a href="' . get_term_link($term) . '">' . $term->name . '</a></li>';
}

public function update ($newInstance, $oldInstance) {
  $update_tax_text = array();
  $update_tax_text["title"] = htmlentities($newInstance["title"]);
  $update_tax_text["category"] = htmlentities($newInstance["category"]);
  $update_tax_text["limit"] = htmlentities($newInstance["limit"]);
  return $update_tax_text;
}

}
//WIDGET LAST POSTS in sidebar

add_action('widgets_init', 'get_last_posts_sidebar');
function get_last_posts_sidebar() {
  register_widget('LastPostsSidebar');
}

class LastPostsSidebar extends WP_Widget {
  function __construct () {
    parent::__construct (
      'Last_posts_sidebar',//ID виджета
      esc_html__('Last Posts Sidebar', 'sg'), //Имя виджета
      array('description'=> esc_html__('Выводит последние посты в сайдбар', 'sg'), )
    );//Описание
  }
  function form ($instance) {
    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'New posts' );
    $count = absint( $instance['count']);
    if ( ! $count ) {
      $count = 5;
    }
    $args_cat = get_categories();
    $active_categories = explode(';', $instance['categories']);
    ?>
    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>"><?= __('Title:', 'sg'); ?></label>
      <input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" value="<?= $title ?>" class="widefat">
    </p>  
    <p>
      <select multiple="multiple" class="widefat" name="<?php echo $this->get_field_name('categories[]'); ?>" id="<?php echo $this->get_field_id('categories'); ?>">
       <?php foreach ($args_cat as $cat){ ?>
       <option value="<?= $cat->term_id ?>"<?php if(in_array($cat->term_id, $active_categories)){ ?> selected="selected"<?php } ?>><?= $cat->cat_name ?></option>
       <?php } ?>
     </select>

   </p>
   <p>
    <label for="<?php echo $this->get_field_id('count'); ?>"><?= __('count:', 'sg'); ?></label>
    <input type="number" name="<?php echo $this->get_field_name('count'); ?>" id="<?php echo $this->get_field_id('count'); ?>" value="<?= $count ?>" class="widefat">
  </p>
  <?php 
}

public function widget ($args, $instance)
{
  $arges = array(
    array(
      'tax_query' => array(
        array(
          'taxonomy' => 'category',
          'field'    => 'id',
          'terms'    => explode(';', $instance['categories'])
          ))),

    'posts_per_page' => $instance['count'],
    'post_type' => 'post',
    'post_status' => 'publish'
    );

  $the_query = new WP_Query($arges);

  ?>
  <?= $args['before_widget'] ?>
  <?= $args['before_title'] ?><?= $instance['title'] ?><?= $args['after_title'] ?>

  <?php
  if ($the_query->have_posts()) {

    while ($the_query->have_posts()) {
      $the_query->the_post();
      echo '<div class="recent_news_block clearfix">';
      echo '<div class="recent_news_img">';
      echo the_post_thumbnail('new_post_img');
      echo ' </div>';
      echo '<div class="recent_news_text">';
      echo '<p><a href=" ' . get_the_permalink() . '">' . get_the_title() . '</p>';
      echo '<span></span>';
      echo '<a class="recent_news_date">'.get_the_date().'</a>';
      echo '</div>';
      echo '</div>';
    }
    /* Restore original Post Data */
    wp_reset_postdata();
  }
  ?>

  <?= $args['after_widget'] ?>

  <?php
}

function update ($newInstance, $oldInstance) {
  $update_last_posts = array();
  $update_last_posts["title"] = htmlentities($newInstance["title"]);
  $update_last_posts["categories"] = implode(';', $newInstance["categories"]);

  $update_last_posts["count"] = htmlentities($newInstance["count"]);
//        $update_last_posts["date"] = htmlentities($newInstance["date"]);
  return $update_last_posts;
}

}
//WIDGET LAST POSTS in Footer

add_action('widgets_init', 'get_last_posts');
function get_last_posts() {
	register_widget('LastPosts');
}

class LastPosts extends WP_Widget {
	function __construct () {
		parent::__construct (
			'last_posts',//ID виджета
			esc_html__('Last Posts', 'sg'), //Имя виджета
			array('description'=> esc_html__('Выводит последние посты в footer', 'sg'), )
		);//Описание
	}
	function form ($instance) {
    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'New posts' );
    $count = absint( $instance['count']);
    if ( ! $count ) {
      $count = 5;
    }
    $args_cat = get_categories();
    $active_categories = explode(';', $instance['categories']);
    ?>
    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>"><?= __('Title:', 'sg'); ?></label>
      <input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" value="<?= $title ?>" class="widefat">
    </p>	
    <p>
      <select multiple="multiple" class="widefat" name="<?php echo $this->get_field_name('categories[]'); ?>" id="<?php echo $this->get_field_id('categories'); ?>">
       <?php foreach ($args_cat as $cat){ ?>
       <option value="<?= $cat->term_id ?>"<?php if(in_array($cat->term_id, $active_categories)){ ?> selected="selected"<?php } ?>><?= $cat->cat_name ?></option>
       <?php } ?>
     </select>

   </p>
   <p>
    <label for="<?php echo $this->get_field_id('count'); ?>"><?= __('count:', 'sg'); ?></label>
    <input type="number" name="<?php echo $this->get_field_name('count'); ?>" id="<?php echo $this->get_field_id('count'); ?>" value="<?= $count ?>" class="widefat">
  </p>
  <?php	
}

public function widget ($args, $instance)
{
  $arges = array(
    array(
      'tax_query' => array(
        array(
          'taxonomy' => 'category',
          'field'    => 'id',
          'terms'    => explode(';', $instance['categories'])
          ))),

    'posts_per_page' => $instance['count'],
    'post_type' => 'post',
    'post_status' => 'publish'
    );

  $the_query = new WP_Query($arges);

  ?>
  <?= $args['before_widget'] ?>
  <?= $args['before_title'] ?><?= $instance['title'] ?><?= $args['after_title'] ?>

  <?php
  if ($the_query->have_posts()) {

    while ($the_query->have_posts()) {
      $the_query->the_post();
      echo '<div class="recent_news_block clearfix">';
      echo '<div class="recent_news_img">';
      echo the_post_thumbnail('new_post_img');
      echo ' </div>';
      echo '<div class="recent_news_text">';
      echo '<p><a href=" ' . get_the_permalink() . '">' . get_the_title() . '</p>';
      echo '<span>Posted on</span>';
      echo '<a class="recent_news_date">'.get_the_date().'</a>';
      echo '</div>';
      echo '</div>';
    }
    /* Restore original Post Data */
    wp_reset_postdata();
  }
  ?>

  <?= $args['after_widget'] ?>

  <?php
}

function update ($newInstance, $oldInstance) {
  $update_last_posts = array();
  $update_last_posts["title"] = htmlentities($newInstance["title"]);
  $update_last_posts["categories"] = implode(';', $newInstance["categories"]);

  $update_last_posts["count"] = htmlentities($newInstance["count"]);
//        $update_last_posts["date"] = htmlentities($newInstance["date"]);
  return $update_last_posts;
}

}

//LAST POST LINK IN FOOTER
add_action('widgets_init', 'get_last_posts_link');
function get_last_posts_link() {
  register_widget('LastPostsLink');
}

class LastPostsLink extends WP_Widget {
  function __construct () {
    parent::__construct (
            'last_posts_link',//ID виджета
            esc_html__('Last Posts Link', 'sg'), //Имя виджета
            array('description'=> esc_html__('Выводит последние посты (список) в сайдбар', 'sg'), )
        );//Описание
  }
  function form ($instance) {
    $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'New posts' );
    $count = absint( $instance['count']);
    if ( ! $count ) {
      $count = 5;
    }
    $args_cat = get_categories();
    $active_categories = explode(';', $instance['categories']);
    ?>
    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>"><?= __('Title:', 'sg'); ?></label>
      <input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" value="<?= $title ?>" class="widefat">
    </p>    
    <p>
      <select multiple="multiple" class="widefat" name="<?php echo $this->get_field_name('categories[]'); ?>" id="<?php echo $this->get_field_id('categories'); ?>">
       <?php foreach ($args_cat as $cat){ ?>
       <option value="<?= $cat->term_id ?>"<?php if(in_array($cat->term_id, $active_categories)){ ?> selected="selected"<?php } ?>><?= $cat->cat_name ?></option>
       <?php } ?>
     </select>

   </p>
   <p>
    <label for="<?php echo $this->get_field_id('count'); ?>"><?= __('count:', 'sg'); ?></label>
    <input type="number" name="<?php echo $this->get_field_name('count'); ?>" id="<?php echo $this->get_field_id('count'); ?>" value="<?= $count ?>" class="widefat">
  </p>
  <?php   
}

public function widget ($args, $instance)
{
  $arges = array(
    array(
      'tax_query' => array(
        array(
          'taxonomy' => 'category',
          'field'    => 'id',
          'terms'    => explode(';', $instance['categories'])
          ))),                

    'posts_per_page' => $instance['count'],
    'post_type' => 'post',
    'post_status' => 'publish'
    );

  $the_query = new WP_Query($arges);

  ?>
  <?= $args['before_widget'] ?>
  <?= $args['before_title'] ?><?= $instance['title'] ?><?= $args['after_title'] ?>
  <div class="resent_post">

    <?php
    if ($the_query->have_posts()) {

      while ($the_query->have_posts()) {
        $the_query->the_post();
        echo '<ul>';
        echo '<li class="recent_post_list">';
        echo '<a class="recent_post_link" href=" ' . get_the_permalink() . '">' . get_the_title() . '</a>';
        echo ' </li>';
        echo '/<ul>';
      }
      /* Restore original Post Data */
      wp_reset_postdata();
    }
    ?>
  </div>

  <?= $args['after_widget'] ?>

  <?php
}

function update ($newInstance, $oldInstance) {
  $update_last_posts_link = array();
  $update_last_posts_link["title"] = htmlentities($newInstance["title"]);
  $update_last_posts_link["categories"] = implode(';', $newInstance["categories"]);

  $update_last_posts_link["count"] = htmlentities($newInstance["count"]);
//        $update_last_posts["date"] = htmlentities($newInstance["date"]);
  return $update_last_posts_link;
}

}

/**
* Post Type: team
*/
function cptui_register_my_cpts_team() {

  $labels = array(
    "name" => __( 'team', '' ),
    "singular_name" => __( 'team', '' ),
    'featured_image' => 'Загрузка миниатюры',
// Миниатюра записи
    'set_featured_image' => 'Установить миниатюру',
// Установить миниатюру записи
    'remove_featured_image' => 'Удаление миниатюры',
// Удалить миниатюру записи
    'use_featured_image' => 'Использовать как миниатюру записи'
// Использовать как миниатюру записи
    );

  $args = array(
    "label" => __( 'team', '' ),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => false,
    "rest_base" => "",
    "has_archive" => false,
    "show_in_menu" => true,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array( "slug" => "team", "with_front" => true ),
    "query_var" => true,
    "supports" => array( "title", "editor", "custom-fields", "thumbnail" ),
    );

  register_post_type( "team", $args );
}

add_action( 'init', 'cptui_register_my_cpts_team' );

/**
* Post Type: Courses
*/
function cptui_register_my_cpts_course() {

  $labels = array(
    "name" => __( 'course', '' ),
    "singular_name" => __( 'courses', '' ),
    'featured_image' => 'Загрузка миниатюры',
// Миниатюра записи
    'set_featured_image' => 'Установить миниатюру',
// Установить миниатюру записи
    'remove_featured_image' => 'Удаление миниатюры',
// Удалить миниатюру записи
    'use_featured_image' => 'Использовать как миниатюру записи'
// Использовать как миниатюру записи
    );

  $args = array(
    "label" => __( 'course', '' ),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => false,
    "rest_base" => "",
    "has_archive" => false,
    "show_in_menu" => true,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array( "slug" => "course", "with_front" => true ),
    "query_var" => true,
    "supports" => array( "title", "editor", "custom-fields", "thumbnail" ),
    );

  register_post_type( "course", $args );
}

add_action( 'init', 'cptui_register_my_cpts_course' );
/**
* Post Type: quotes
*/
function cptui_register_my_cpts_quotes() {

  $labels = array(
    "name" => __( 'quotes', '' ),
    "singular_name" => __( 'quotes', '' ),
    );

  $args = array(
    "label" => __( 'quotes', '' ),
    "labels" => $labels,
    "description" => "",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => false,
    "rest_base" => "",
    "has_archive" => false,
    "show_in_menu" => true,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => array( "slug" => "quotes", "with_front" => true ),
    "query_var" => true,
    "supports" => array( "title", "editor", "custom-fields" ),
    );

  register_post_type( "quotes", $args );
}

add_action( 'init', 'cptui_register_my_cpts_quotes' );
