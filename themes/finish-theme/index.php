<?php get_header(); ?>
<!--SLIDER ON TOP-->
<?php masterslider(2); ?>
<!--After slider block-->
<section class="after_slider_wrap">
	<div class="container">
		<div class="row">
			<?php dynamic_sidebar('after_slider_block'); ?>
		</div>
	</div>
</section>
<!--RISUS MAGNA-->
<section class="risus_magna">
	<div class="container">
		<div class="row">
			<div class="risus_magna_wrap">
				<?php 
				$content_courses = get_field('content_courses');
				$risus_magna_args = array(
					'post_type' => 'course',
					'post_status' => 'publish',
					'posts_per_page'  => 1,
					'order' => 'ACS'
					);
				$risus_magna = new WP_Query($risus_magna_args);
				?>
				<?php if($risus_magna->have_posts()) : ?>
					<?php while ($risus_magna->have_posts()) : $risus_magna->the_post(); ?>
						<div class="col-lg-10">
							<div class="risus_magna_text">
								<h2><?php the_title(); ?></h2>
								<p><?php the_field('content_courses'); ?></p>
							</div>
						</div>
						<div class="col-lg-2">
							<div class="risus_magna_submit">
								<a class="risus_magna_link" href="<?php the_permalink(); ?>"><?=__('Learn more', 'sg')  ?></a>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<!--Recent Courses-->
<section class="slider">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="courses_title">
					<i class="fa fa-angle-left icon-angle-left"></i>
					<h3><?=__('Recent Courses', 'sg') ?></h3>
					<i class="fa fa-angle-right icon-angle-right"></i>
				</div>
				<div class="courses_link">
					<a href="<?php the_permalink(); ?>"><?='View All Courses'?></a>
				</div>
			</div>
			<div class="courses_slider_block clearfix">
				<?php  
				$title_courses_post = get_field( "title_courses_post" ); 
				$prices_courses = get_field( "prices_courses" ); 
				$date_published_post = get_field( "date_published_post" ); 

				$args_recent_courses = array(
					'post_type' => 'course',
					'post_status' => 'publish',
					'post__in'  => get_option( 'sticky_posts' ),
					'ignore_sticky_posts' => 1
					);
				$recent_courses = new WP_Query($args_recent_courses);
				?>
				<?php if($recent_courses->have_posts()) : ?>
					<?php while ($recent_courses->have_posts()) : $recent_courses->the_post(); ?> 

						<div class="archive_content_block">
							<div class="col-lg-4">
								<!-- <div class="swiper-container" id="swiper-container">
									<div class="swiper-wrapper">
										<div class="swiper-slide"> -->
											<div class="courses_slider_content">
												<a href="<?php the_permalink(); ?>">
													<?php 
													$id = get_post_thumbnail_id();
													$images_courses_posts = wp_get_attachment_image_src( $id, array(360,205) );
													if( !empty($images_courses_posts) ): ?>
													<img src="<?php echo $images_courses_posts[0]; ?>" width= "<?php $images_courses_posts[1]; ?>" height= "<?php $images_courses_posts[2]; ?>" />
												<?php endif; ?>
											</a>
										</div>
										<h3><a href="<?php the_permalink(); ?>"><?php the_field('title_courses_post'); ?></a></h3>
										<div class="courses_slider_bottom">
											<span class="price"><?php the_field('prices_courses') ?></span>
											<div class="courses_slider_date">
												<i class="fa fa-clock-o icon-time"></i>
												<span class="date"><?php the_time('j F Y'); ?></span>
											</div>
										</div>
						<!-- 			</div>
								</div>
							</div> -->
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
</section>
<!--Sidebar-->
<?php get_sidebar(); ?>
<!--Quotes-->
<section class="quotes">
	<div class="container">
		<div class="row">
			<div class="quotes_title">
				<i class="fa fa-angle-left icon-angle-left"></i>
				<h3><?= __('What student say?', 'sg') ?></h3>
				<i class="fa fa-angle-right icon-angle-right"></i>
			</div>
			<div class="quotes_divider"></div>

			<?php 
			$quote_text = get_field( "quote_text" ); 
			$quotes_name = get_field( "quotes_name" ); 
			$quotes_job = get_field( "quotes_job" ); 
			?>
			<?php 
			$args = array(
				'post_type' => 'quotes',
				'posts_per_page' => 1,
				'orderby' => 'rand'
				);
			$quotes = new WP_Query($args);
			if ( $quotes->have_posts() ) {
				while ( $quotes->have_posts() ) {
					$quotes->the_post();
					?>
					<div class="quote_text">
						<div class="swiper-container" id="swiper-container3">
							<div class="swiper-wrapper">
								<div class="swiper-slide">
									<p><?php the_field('quote_text'); ?></p>
									<span class="quotes_name"><?php the_field('quotes_name'); ?></span>
									<span class="quotes_job"><?php the_field('quotes_job'); ?></span>
								</div>
								<div class="swiper-slide">
									<p><?php the_field('quote_text'); ?></p>
									<span class="quotes_name"><?php the_field('quotes_name'); ?></span>
									<span class="quotes_job"><?php the_field('quotes_job'); ?></span>
								</div>
								<div class="swiper-slide">
									<p><?php the_field('quote_text'); ?></p>
									<span class="quotes_name"><?php the_field('quotes_name'); ?></span>
									<span class="quotes_job"><?php the_field('quotes_job'); ?></span>
								</div>
							</div>
						</div>
					</div>
					<?php
				}
			} else {
			}
			/* Возвращаем оригинальные данные поста. Сбрасываем $post. */
			wp_reset_postdata();
			?>
		</div>
	</div>
</section>
<!--Popular-->
<section class="popular">
	<div class="container">
		<div class="row">
			<div class="popular_title">
				<h3><?=__('Check out some Popular courses', 'sg') ?></h3>
			</div>
			<div class="quotes_divider"></div>
			<p class="popular_text"><?=__('Tellus Elit Euismod Ultricies', 'sg')?></p>

			<?php 
			$args = array(
				'posts_per_page' => 3,
				'orderby' => 'comment_count'
				);
			$the_query = new WP_Query( $args );

			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				?>
				<div class="col-lg-4">
					<div class="popular_img">
						<a href="<?php the_permalink(); ?>">
							<?php 
							$id_post = get_post_thumbnail_id();
							$popular_posts = wp_get_attachment_image_src( $id_post, array(360, 200) );
							if( !empty($popular_posts) ): ?>
							<img src="<?php echo $popular_posts[0]; ?>" width="<?php echo $popular_posts[1] ?>" height="<?php echo $popular_posts[2] ?>" />
						<?php endif; ?>
					</a>
				</div>	
			</div>
			<?php	
		}
		wp_reset_postdata();
		?>
	</div>
</div>
</section>
<!--Twitter-->
<section class="twitter">
	<div class="container">
		<div class="row">
			<div class="twitter_title">
				<i class="fa fa-angle-left icon-angle-left"></i>
				<h3><i class="fa fa-twitter icon-twitter"></i></h3>
				<i class="fa fa-angle-right icon-angle-right"></i>
			</div>
			<div class="twitter_slide_text">
				<div class="swiper-container" id="swiper-container2">
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<?php dynamic_sidebar( 'twitter'); ?>
						</div>
						<div class="swiper-slide">
							<?php dynamic_sidebar( 'twitter'); ?>
						</div>
						<div class="swiper-slide">
							<?php dynamic_sidebar( 'twitter'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
<script>
	var swiper = new Swiper('#swiper-container', {
		slidesPerView: 3,
		paginationClickable: true,
		nextButton: '.icon-angle-right',
		prevButton: '.icon-angle-left',
		spaceBetween: 30,
	});
	var swiper2 = new Swiper('#swiper-container2', {
		slidesPerView: 1,
		speed: 400,
		paginationClickable: true,
		nextButton: '.icon-angle-right',
		prevButton: '.icon-angle-left',
		spaceBetween: 30,
	});
	var swiper = new Swiper('#swiper-container3', {
		slidesPerView: 1,
		speed: 400,
		paginationClickable: true,
		nextButton: '.icon-angle-right',
		prevButton: '.icon-angle-left',
		spaceBetween: 30,
	});
</script>