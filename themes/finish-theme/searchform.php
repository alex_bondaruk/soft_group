<form class="search" role="search" method="get" action="<?php echo home_url( '' ) ?>">
	<i class="fa fa-search icon-search"></i>
	<!-- <input type="submit" id="searchsubmit"> -->
	<div class="search_text" id="search_text">
		<input type="text" value="<?php echo get_search_query() ?>" name="s" id="s">
	</div>
</form>
