<?php
/*
Template Name: Archives (Blog)
*/
?>
<?php get_header(); ?>
<div class="posts_block_wrap">
	<div class="posts_block"></div>
	<div class="posts_block_text">
		<h1><?php wp_title(); ?></h1>
	</div>
</div>
<section class="archive_content clearfix">
	<div class="container">
		<div class="row">
			<?php  
			$args_blog = array(
				'post_type' => 'post',
				'post_per_page'=> 9
			);
			$blog = new WP_Query($args_blog);
			?>
			<?php if($blog->have_posts()) : ?>
				<?php while ($blog->have_posts()) : $blog->the_post(); ?> 	
					<div class="col-lg-4">
						<div class="archive_content_block clearfix">
							<article class="archive_posts">
								<div class="archive_post_thumbnail">
									<a href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail();?>
									</a>
									<!-- <div class="archive_post_banner">
										<i class="fa fa-bullhorn"></i>
										Sticky Post
									</div> -->
								</div>
								<div class="archive_post_header">
									<h3 class="archive_post_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<div class="archive_post_info">
										<div class="archive_post_info_date">
											<span><?= __('Posted on', 'sg') ?></span>
											<a href="<?php the_permalink(); ?>"><?php the_time('j F Y'); ?></a>
										</div>
										<div class="archive_post_info_tag">
											<span><?= __('Tags', 'sg') ?></span>
											<a href="<?php the_permalink(); ?>"><?php the_tags(); ?></a>
										</div>
										<div class="archive_post_info_comment">
											<span><?= __('Comments', 'sg') ?></span>
											<a href="<?php the_permalink(); ?>"><?php comments_number(); ?></a>
										</div>
									</div>
								</div>
								<div class="archive_post_content">
									<p><?php the_excerpt(); ?></p>
										<a href="<?php the_permalink(); ?>" class="read_more"><?=__('Read more', 'sg') ?></a>
									</div>
								</article>
							</div>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</section>
	<?php get_footer(); ?>
