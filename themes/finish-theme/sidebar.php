<div class="school_courses_wrap">
	<div class="container">
		<div class="row">
			<!-- Recent news -->
			<div class="recent_news">
				<div class="col-lg-4">
					<!-- <h3>Recent news</h3>
					<a class="recent_news_link" href="#">/ Read The Blog</a> -->
					<?php dynamic_sidebar('left_sidebar_new'); ?>
				</div>
			</div>
			<!-- Video -->
			<div class="school_course_block">
				<div class="col-lg-4">
					<?php dynamic_sidebar('center_sidebar_new'); ?>
				</div>
			</div>
			<!-- Random team post -->
			<div class="col-lg-4">
				<?php 
				$name_worker = get_field( "name_worker" ); 
				$job_worker_from_team = get_field( "job_worker_from_team" ); 
				$main_text = get_field( "main_text" ); 
				$link_team = get_field( "link_team" ); 
				?>

				<?php 
				$args = array(
					'post_type' => 'team',
					'posts_per_page' => 1,
					'orderby' => 'rand'
					);
				$team = new WP_Query($args);
				if ( $team->have_posts() ) {
					while ( $team->have_posts() ) {
						$team->the_post();
						?>
						<div class="school_instructor_aside">
							<h3><?php the_title(); ?></h3>
							<div class="school_instructor_backgroung">
								<div class="school_instructor_img">
									<?php 
									$id_posts = get_post_thumbnail_id();
									$images_team_posts = wp_get_attachment_image_src( $id_posts, array(300,300) );
									if( !empty($images_team_posts) ): ?>
									<img src="<?php echo $images_team_posts[0]; ?>" width= "<?php $images_team_posts[1]; ?>" height= "<?php $images_team_posts[2]; ?>" />
								<?php endif; ?>
							</div>
							<p class="school_courses_name"><?php the_field('name_worker'); ?></p>
							<p class="school_courses_job"><?php the_field('job_worker_from_team'); ?></p>
							<p class="school_courses_text"><?php the_field('main_text'); ?></p> 
							<a href="#" class="read_more"><?php the_field('link_team'); ?></a>
						</div>
					</div>
				</div>
				<?php
			}
		} else {				
		}
		wp_reset_postdata();
		?>
	</div>
</div>
</div>	


