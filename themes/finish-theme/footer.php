<div class="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<?php dynamic_sidebar('about_footer'); ?>
			</div>
			<div class="col-lg-3">
				<?php dynamic_sidebar('left'); ?>
			</div>
			<div class="col-lg-3">
				<div class="tag_cloud">
					<!-- <h3>Tag cloud</h3> -->
					<div class="tag_cloud_block">
						<?php dynamic_sidebar('tag_cloud'); ?>
					</div>
					
				</div>
			</div>
			<div class="col-lg-3">
				<div class="recent_post_list_block">
					<?php dynamic_sidebar('footer'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="footer_bottom">
		<div class="container">
			<div class="footer_bottom_left clearfix">
				<?php dynamic_sidebar('footer_left'); ?>
			</div>
			<div class="footer_bottom_right clearfix">
				<?php dynamic_sidebar('footer_right'); ?>
			</div>
		</div>
	</div>
</div>
<?php wp_footer(); ?>  
</div>
</body>
</html>