<?php
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>    
		<section class="contact_block">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<?php the_content(); ?>
					</div>
					<div class="col-lg-4">
						<?php dynamic_sidebar('contact_sidebar'); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php endwhile; ?>
<?php endif; ?>

<!-- </section>
<div class="contacts_img_block">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="contacts_img_blocks">
					<div class="contacts_img_blocks_wrap">
						<div class="contacts_img_circle_icon">
							<i class="icon-envelope fa fa-envelope"></i><br>
						</div>
						<h4 class="box_with_icon_title">Contact By Email</h4>
						<div class="contacts_img_icon_text">
							<p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="contacts_img_blocks">
					<div class="contacts_img_blocks_wrap">
						<div class="contacts_img_circle_icon">
							<i class="icon-phone-sign fa fa-phone-sign"></i><br>
						</div>
						<h4 class="box_with_icon_title">Contact By Phone</h4>
						<div class="contacts_img_icon_text">
							<p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="contacts_img_blocks">
					<div class="contacts_img_blocks_wrap">
						<div class="contacts_img_circle_icon">
							<i class="icon-home fa fa-home"></i><br>
						</div>
						<h4 class="box_with_icon_title">Come To See Us</h4>
						<div class="contacts_img_icon_text">
							<p>Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>				
</div> -->
<?php get_footer(); ?>