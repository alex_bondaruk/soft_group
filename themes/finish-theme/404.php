<?php get_header(); ?>
<section class="error">
	<div class="posts_block_wrap">
		<div class="posts_block"></div>
		<div class="posts_block_text">
			<h1><?= __('404', 'sg') ?></h1>
			<p><?=__('Page not found', 'sg') ?></p>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="error_red_block">
				<div class="page_not_found_icon">
					<i class="fa fa-frown-o icon-frown"></i>
				</div>
				<p class="error_red_title"><?=__('Error 404', 'sg')?></p>
				<p class="error_red_text"><?=__("Sorry, we couldn't find the page you're looking for.", 'sg') ?></p>
				<div class="page_not_found_search">
					<div class="error_search_form">
						<form method="get" id="searchform" action="<?php echo home_url() ?>">
							<div class="search_text" id="search-text">
								<input type="text" name="s" id="s" value="Type keywords...">
							</div>
							<input type="submit" id="searchsubmit" value="">
						</form>
					</div>				
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>