<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php bloginfo('name'); wp_title(); ?></title>
    <?php wp_head(); ?>
    <script>
        jQuery(document).ready(function($) {      
            $(".nav_menu_search i").click(function() {
                $(".search_wrap_hidden").toggle();
            });
        });
    </script>
</head>
<body>
    <div class="wrapper">
        <!--HEADER-->
        <header class="wrap">
            <div class="header_top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <?php if(!dynamic_sidebar('left_top_header')): ?>  
                                <p>Место под телефон и емейл</p>  
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-6">
                            <?php dynamic_sidebar('right_top_header'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 clearfix">
                        <div class="logo">
                            <a href="<?php echo home_url(); ?>">
                                <img src="<?php bloginfo('template_url') ?>/images/logo.png" alt="Clever course">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-8 clearfix">
                        <?php wp_nav_menu(array(
                            'theme_location' => 'primary',
                            'container'       => 'nav', 
                            'container_class' => 'nav', 
                            'menu_class' => 'menu',

                            ));

                            ?>
                            <div class="nav_menu_search">
                                <i class="fa fa-search icon-search"></i> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="search_wrap_hidden">
                    <div class="container">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </header>