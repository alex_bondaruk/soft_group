<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>    

		<section class="post">
			<div class="posts_block_wrap">
				<div class="posts_block"></div>
				<div class="posts_block_text">
					<h1><?php wp_title(''); ?></h1>
				</div>
			</div>
			<div class="container">
				<?php the_content(); ?>
			</div>
			


			<!-- <?php comments_template( 'comments.php' ); ?>
			<?php get_comment_excerpt(); ?> -->
		<?php endwhile; ?>
	<?php endif; ?>