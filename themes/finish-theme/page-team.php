<?php
/*
Template Name: Team
*/
?>
<?php get_header(); ?>
<?php 
$name_worker = get_field( "name_worker" ); 
$job_worker_from_team = get_field( "job_worker_from_team" ); 
$main_text = get_field( "main_text" ); 
$link_team = get_field( "link_team" ); 
?>
<section class="instructor_list">
	<div class="posts_block_wrap">
		<div class="posts_block"></div>
		<div class="posts_block_text">
			<h1><?php wp_title(''); ?></h1>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="courses_slider_block clearfix">
				<?php 
				$args = array('post_type' => 'team');
				$team = new WP_Query($args);
				if ( $team->have_posts() ) {
					while ( $team->have_posts() ) {
						$team->the_post();
						?>
						<div class="col-lg-4">
							<div class="school_instructor">
								<div class="school_instructor_backgroung">
									<div class="school_instructor_img">
										<?php 
										$img = get_field( "post_thumbnail" ); 
										if( !empty($img) ): ?>
										<img src="<?php echo $img['url']; ?>" />
									<?php endif; ?>
								</div>
								<p class="school_courses_name"><?php the_field('name_worker'); ?></p>
								<p class="school_courses_job"><?php the_field('job_worker_from_team'); ?></p>
								<p class="school_courses_text"><?php the_field('main_text'); ?></p> 
								<a href="#" class="read_more"><?php the_field('link_team'); ?></a>
							</div>
						</div>
					</div>
					<?php
				}
			} else {
						// Постов не найдено
			}
			/* Возвращаем оригинальные данные поста. Сбрасываем $post. */
			wp_reset_postdata();
			?>
		</div>
	</div>
</div>
</section>	
<?php get_footer(); ?>