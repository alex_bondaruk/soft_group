<?php get_header(); ?>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                  
                   <h1 class="page-header"> 
						<?php bloginfo(); ?>
						<small><?php bloginfo('description'); ?></small>
					</h1>
                   

                    <?php get_template_part( 'loop' ); ?>

					</div>

				   <div class="col-md-4">
						<?php get_sidebar() ?>
					</div>
               
                </div>
<?php get_footer(); ?>