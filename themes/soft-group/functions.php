<?php
/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain('sg', get_template_directory() . '/languages');



/**
 * Додаємо favicon, charset, viewport
 */
function add_head_meta_tags()
{
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favicon.png" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <?php
}
add_action('wp_head', 'add_head_meta_tags');


/**
 * Реєструємо місце для меню
 */

register_nav_menus( array(
    'primary' => __('Primary', 'sg'),
) );


/**
 * Реєструємо сайдбари теми
 */
if ( function_exists('register_sidebar') )
{
    register_sidebars(1, array(
        'id' => 'left',
        'name' => __('Left sidebar', 'sg'),
        'description' => '',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));

	register_sidebars(1, array(
		'id' => 'footer',
        'name' => __('Footer', 'sg'),
        'description' => '',
		'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
		'before_title' => '<p>',
        'after_title' => '</p>'
	));
}


/**
 * Підключаємо підтримку мініатюр
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
//		set_post_thumbnail_size( 900, 300 );
}
/**
// * Можемо добавити різні розміри для картинок
 */
if ( function_exists( 'add_image_size' ) ) { 
    add_image_size( 'imgpost', 900, 300, true );
}
 	add_image_size( 'last_post_img', 150, 100, true );

/**
 * Реєструємо формати постів
 */
function add_post_formats(){
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'add_post_formats', 11 );


/**
 * Удаляємо стандартне закінчення обрізаного тексту з [...] 
 */
function custom_excerpt_more( $more ) {
	return '';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );


/**
 * Замінюємо стандартну довжину обрізаного тексту
 */
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


/**
 * Підключаємо javascript файли
 */
function add_theme_scripts(){
    wp_enqueue_script("jquery");
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
    wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array('jquery', 'bootstrap') );
	wp_enqueue_script( 'npm', get_template_directory_uri() . '/js/npm.js', array('jquery', 'bootstrap') );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


/**
 * Підключаємо css файли
 */
function add_theme_style(){
    //wp_enqueue_style( 'fonts', get_template_directory_uri() . '/fonts/stylesheet.css');
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
	wp_enqueue_style( 'bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css');
	wp_enqueue_style( 'blog-home', get_template_directory_uri() . '/css/blog-home.css');
	wp_enqueue_style( 'font', get_template_directory_uri() . '/css/font.css');
}
add_action( 'wp_enqueue_scripts', 'add_theme_style' );

add_action('test', 'test');
function test( $a ){
    return '1';
}

add_filter('test', 'test2');
function test2( $a ){
    return '2';
}

/*Filter Link*/

add_filter('the_content', 'my_link_filter');

function my_link_filter($content) {
  	 include_once ('simple_html_dom.php');	
	 $site_url = parse_url(get_site_url());
	 $html = str_get_html($content);
     $link_c = $html->find('a');
		
	foreach ($link_c as $link) {
	$part_url = parse_url($link->href);
	if( !isset($part_url['host']) || $part_url['host'] == $site_url['host'] ){
		continue;
	}
	$link->target = '_blank';
	$link->href = '/redirect?to=' . $link->href;
	}

	return $html;
} 

add_action('wp_loaded', 'redirect_links'); 
function redirect_links(){
    if( strpos($_SERVER['REQUEST_URI'], '/redirect') === 0 && isset($_GET['to'])){
        wp_redirect( $_GET['to'], 302 );
        die;
    }
};

/* SHARE SOCIALS */
add_filter('the_content', 'addSocialShareLinks');
function addSocialShareLinks( $content ){
    global $post;
    $params = [
        '{url}',
        '{img}',
        '{title}',
        '{desc}',
        '{app_id}',
        '{redirect_url}',
        '{via}',
        '{hashtags}',
        '{provider}',
        '{is_video}'
    ];

    $values = [
        get_permalink(),
        get_the_post_thumbnail_url('full'),
        get_the_title(),
        $post->post_content,
        '', //'{app_id}'
        get_site_url(),
        '', //'{via}'
        '', //{hashtags}
        '', //{provider}
        '', //{is_video}
    ];

    $links = [
        'twitter' => 'https://twitter.com/intent/tweet?url={url}&text={title}&via={via}&hashtags={hashtags}',
        'google' => 'https://plus.google.com/share?url={url}',
        'facebook' => 'https://www.facebook.com/share.php?url={url}',
    ];

    $linksString = '';
    foreach ($links as $key => $link){
        $linksString .= '<a class="'. $key . '" href="' . str_replace($params, $values, $link) . '"></a>';
    }

    return $content . '<div class="social_links">' . $linksString . '</div>';
};


/* СТВОРЕННЯ ВІДЖЕТА */
add_action('widgets_init', 'category_widg');

function category_widg() {
	register_widget('Category_Tax');
}


class Category_Tax extends WP_Widget {
	function __construct () {
		parent::__construct (
			'category_tax',//ID виджета
			esc_html__('CategoryTax', 'sg'), //Имя виджета
			array('description'=> esc_html__('Выводит категории в сайдбар', 'sg'), )
		);//Описание
	}
	
	public function form ($instance) {
		$title = '';
		$category = null;
        $limit = 16;
		
		?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?= __('Title:', 'sg'); ?></label>
				<input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" value="<?= $title ?>" class="widefat">
			</p>	
			
			 <p>
				<select class="widefat" name="<?php echo $this->get_field_name('category'); ?>" id="<?php echo $this->get_field_id('category'); ?>">
					<?php foreach (get_taxonomies(['show_ui' => true, 'show_in_nav_menus' => true, '_builtin' => true], 'object') as $tax){ ?>
						<option value="<?= $tax->name ?>"<?php if($category == $tax->name){ ?> selected="selected"<?php } ?>><?= $tax->label ?></option>
					<?php } ?>
				</select>
			</p>
			
			<p>
				<label for="<?php echo $this->get_field_id('limit'); ?>"><?= __('Limit:', 'sg'); ?></label>
				<input type="number" name="<?php echo $this->get_field_name('limit'); ?>" id="<?php echo $this->get_field_id('limit'); ?>" value="<?= $limit ?>" class="widefat">
			</p>
		<?php
	}
	
public function widget ($args, $instance) {	
		
		$terms = get_terms([
            'taxonomy' => [ $instance['category'] ],
            'number' => $instance['limit']
        ]);
        $termsLists = [
            0 => [],
            1 => []
        ];
        foreach ($terms as $key => $term){
            if( $key % 2 == 0 ){
                $termsLists[0][] = $term;
            } else {
                $termsLists[1][] = $term;
            }
        }
		?>
        <?= $args['before_widget'] ?>
            <?= $args['before_title'] ?><?= $instance['title'] ?><?= $args['after_title'] ?>
            <div class="row">
                <div class="col-lg-6">
                    <ul class="list-unstyled">
                        <?php foreach ($termsLists[1] as $term){
                            echo $this->get_tax_item( $term );                          
                        } ?>
                    </ul>
                </div>
                <!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <ul class="list-unstyled">
                        <?php foreach ($termsLists[0] as $term){
                            echo $this->get_tax_item( $term );
                        } ?>
                    </ul>
                </div>
                <!-- /.col-lg-6 -->
            </div>
            
        <?= $args['after_widget'] ?>
        <?php
	}
	 public function get_tax_item( $term )
		{
			return '<li><a href="' . get_term_link($term) . '">' . $term->name . '</a></li>';
		}
	
	public function update ($newInstance, $oldInstance) {
		$update_tax_text = array();
        $update_tax_text["title"] = htmlentities($newInstance["title"]);
        $update_tax_text["category"] = htmlentities($newInstance["category"]);
        $update_tax_text["limit"] = htmlentities($newInstance["limit"]);
        return $update_tax_text;
	}
	
}

//WIDGET LAST POSTS

add_action('widgets_init', 'get_last_posts');
function get_last_posts() {
	register_widget('LastPosts');
}

class LastPosts extends WP_Widget {
	function __construct () {
		parent::__construct (
			'last_posts',//ID виджета
			esc_html__('Last Posts', 'sg'), //Имя виджета
			array('description'=> esc_html__('Выводит последние посты в сайдбар', 'sg'), )
		);//Описание
	}
	function form ($instance) {
        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'New posts' );
        $count = absint( $instance['count']);
        if ( ! $count ) {
            $count = 5;
        }
        $args_cat = get_categories();
        $active_categories = explode(';', $instance['categories']);
//        $tags_order = get_tags();
//        $active_tags = explode(';', $instance['tags']);
//        echo "<pre>";
//        var_dump($tags_order);
//        echo "</pre>";
		?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?= __('Title:', 'sg'); ?></label>
				<input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" value="<?= $title ?>" class="widefat">
			</p>	
			 <p>
				<select multiple="multiple" class="widefat" name="<?php echo $this->get_field_name('categories[]'); ?>" id="<?php echo $this->get_field_id('categories'); ?>">
					<?php foreach ($args_cat as $cat){ ?>
						<option value="<?= $cat->term_id ?>"<?php if(in_array($cat->term_id, $active_categories)){ ?> selected="selected"<?php } ?>><?= $cat->cat_name ?></option>
					<?php } ?>
				</select>

			</p>
<!--
            <p>
                <select multiple="multiple" class="widefat" name="<?php echo $this->get_field_name('tags'); ?>" id="<?php echo $this->get_field_id('tags'); ?>">
                    <?php foreach ($tags_order as $tag){ ?>
                        <option value="<?= $tag->tag_id ?>"<?php if($tag->tag_id){ ?> selected="selected"<?php } ?>><?= $tag->tag ?></option>
                    <?php } ?>
                </select>

            </p>
-->
			<p>
				<label for="<?php echo $this->get_field_id('count'); ?>"><?= __('count:', 'sg'); ?></label>
				<input type="number" name="<?php echo $this->get_field_name('count'); ?>" id="<?php echo $this->get_field_id('count'); ?>" value="<?= $count ?>" class="widefat">
			</p>
		<?php	
	}

	public function widget ($args, $instance)
    {
        $arges = array(
                array(
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'category',
                            'field'    => 'id',
                            'terms'    => explode(';', $instance['categories'])
                        ))),
//                    'tax_query' => array(
//                        array(
//                            'taxonomy' => 'tag',
//                            'field'    => 'id',
//                            'terms'    => $instance['tags']
//                        ))),

            'posts_per_page' => $instance['count'],
//            'date_query' => array(
//                    array(
//                        'year'  => $date['year'],
//                        'month' => $date['mon'],
//                        'day'   => $date['mday'],
//                    ),
//                ),
            'post_type' => 'post',
            'post_status' => 'publish'
        );

        $the_query = new WP_Query($arges);

        ?>
        <?= $args['before_widget'] ?>
        <?= $args['before_title'] ?><?= $instance['title'] ?><?= $args['after_title'] ?>
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-unstyled">
	
                    <?php
                    if ($the_query->have_posts()) {

                        while ($the_query->have_posts()) {
                            $the_query->the_post();
								echo '<a href=" ' . get_the_permalink() . '">' .the_post_thumbnail('last_post_img');
                                echo '<li><a href=" ' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
                            }
                        /* Restore original Post Data */
                        wp_reset_postdata();
                    }
                    ?>
                </ul>
            </div>
        </div>

        <?= $args['after_widget'] ?>

        <?php
    }
	
	function update ($newInstance, $oldInstance) {
		$update_last_posts = array();
        $update_last_posts["title"] = htmlentities($newInstance["title"]);
        $update_last_posts["categories"] = implode(';', $newInstance["categories"]);

        $update_last_posts["count"] = htmlentities($newInstance["count"]);
//        $update_last_posts["date"] = htmlentities($newInstance["date"]);
        return $update_last_posts;
	}

}


///* Створення нового типа записей tweet */
//add_action('init', 'my_type_posts');
//function my_type_posts(){
//	register_post_type('tweet', array(
//		'labels'             => array(
//			'name'               => 'Tweets', // Основное название типа записи
//			'singular_name'      => 'Tweet', // отдельное название записи типа Tweet
//			'add_new'            => 'Добавить tweet',
//			'add_new_item'       => 'Добавить новый tweet',
//			'edit_item'          => 'Редактировать tweet',
//			'new_item'           => 'Новый tweet',
//			'view_item'          => 'Посмотреть tweet',
//			'search_items'       => 'Найти tweet',
//			'not_found'          => 'Tweet не найдено',
//			'parent_item_colon'  => '',
//			'menu_name'          => 'Tweets'
//
//		  ),
//		'public'             => true,
//		'publicly_queryable' => true,
//		'show_ui'            => true,
//		'show_in_menu'       => true,
//		'query_var'          => true,
//		'rewrite'            => true,
//		'capability_type'    => 'post',
//		'has_archive'        => true,
//		'hierarchical'       => false,
//		'menu_position'      => null,
//		'supports'           => array('title','editor','author','thumbnail','excerpt','comments')
//	) );
//}

function cptui_register_my_cpts_tweets() {

    /**
     * Post Type: tweet.
     */

    $labels = array(
        "name" => __( 'tweet', '' ),
        "singular_name" => __( 'tweet', '' ),
    );

    $args = array(
        "label" => __( 'tweet', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "tweets", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "custom-fields" ),
    );

    register_post_type( "tweets", $args );
}

add_action( 'init', 'cptui_register_my_cpts_tweets' );


//WIDGET GET TWEETS WITH TWITTER

/*add_action('widgets_init', 'get_tweets');
function get_tweets() {
	register_widget('GetTweets');
}*/
/*
class GetTweets extends WP_Widget {
	function __construct () {
		parent::__construct (
			'get_tweets',//ID виджета
			esc_html__('Get Tweets', 'sg'), //Имя виджета
			array('description'=> esc_html__('Выводит твиты с twitter', 'sg'), )
		);//Описание
	}
	function form ($instance) {
        $title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'New tweets' );
//		$args_cat = get_categories();
//        $active_categories = explode(';', $instance['categories']);
		
//        echo "<pre>";
//        var_dump($tags_order);
//        echo "</pre>";
		?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>"><?= __('Title:', 'sg'); ?></label>
				<input type="text" name="<?php echo $this->get_field_name('title'); ?>" id="<?php echo $this->get_field_id('title'); ?>" value="<?= $title ?>" class="widefat">
			</p>	
			 <p>
				<select multiple="multiple" class="widefat" name="<?php echo $this->get_field_name('tweet'); ?>" id="<?php echo $this->get_field_id('tweet'); ?>">
					<?php foreach ($args_cat as $cat){ ?>
						<option value="<?= $cat->term_id ?>"<?php if(in_array($cat->term_id, $active_categories)){ ?> selected="selected"<?php } ?>><?= $cat->cat_name ?></option>
					<?php } ?>
				</select>

			</p>
		<?php	
	}

	public function widget ($args, $instance)
    {
        $arges = array(
//			 array(
//				'tax_query' => array(
//					array(
//						'taxonomy' => 'category',
//						'field'    => 'id',
//						'terms'    => explode(';', $instance['categories'])
//					))),
            'post_type' => 'tweet',
        );
//		$posts = get_posts( $arges );

		
        $the_query = new WP_Query($arges);

        ?>
        <?= $args['before_widget'] ?>
        <?= $args['before_title'] ?><?= $instance['title'] ?><?= $args['after_title'] ?>
        <div class="row">
            <div class="col-lg-12">
                <ul class="list-unstyled">

                    <?php
                    if ($the_query->have_posts()) {

                        while ($the_query->have_posts()) {
                            $the_query->the_post();
                                echo '<li><a href=" ' . get_the_permalink() . '">' . get_the_title() . '</a></li>';
                            }
                        /* Restore original Post Data 
                        wp_reset_postdata();
                    }
                    ?>
                </ul>
            </div>
        </div>

        <?= $args['after_widget'] ?>

        <?php
    }
	
	function update ($newInstance, $oldInstance) {
		$update_last_posts = array();
        $update_last_posts["title"] = htmlentities($newInstance["title"]);
//		$update_last_posts["categories"] = implode(';', $newInstance["categories"]);
        return $update_last_posts;
	}

}*/
