<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>    
       <h2>
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		</h2>
		<p class="lead">
			<?= __('by', 'sg') ?> <a href="<?php the_permalink(); ?>"><?php the_author_posts_link(); ?></a>
		</p>
		<p><span class="glyphicon glyphicon-time"></span> <?= __('Posted on', 'sg') ?> <?php the_time('F j, Y \a\t H:i'); ?></p>
		<hr>
		<?php the_post_thumbnail( 'imgpost' ); ?>
		<hr>
		<?php the_content(); ?>
		<a class="btn btn-primary" href="<?php the_permalink(); ?>"><?= __('Read More', 'sg') ?><span class="glyphicon glyphicon-chevron-right"></span></a>

		<hr>
    <?php endwhile; ?>
<?php endif; ?>