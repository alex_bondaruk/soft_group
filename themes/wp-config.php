<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'sgdevlab_wp01hm');

/** MySQL database username */
define('DB_USER', 'sgdevlab_wp01hm');

/** MySQL database password */
define('DB_PASSWORD', 'o73mKIzGzhfc');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l0J*jRUQ+!Vl-n%.A%`8SQ7%,|X.v.?5_`3hhw2OMO>{qDWb+.Yd=Ul<y6 8n4Z)');
define('SECURE_AUTH_KEY',  '=dFSmnQa^?%P^=FS}#52CDW1^)kPO?vv,Q![nh, msiqhjuTgY.Z$cfrfFr]cz{y');
define('LOGGED_IN_KEY',    'uw%ly:I;jD#./-#yw%Yjns(pEG S3t8]zbK]V?Qwm5J[0W )gWaFnJS[iFoS!xl|');
define('NONCE_KEY',        '^n=#NRF9g@qs=jKPclM3-/?z#:@xUq)je<3Nq @M>ZhU $fl3Ab`$G_hIeo 8M[-');
define('AUTH_SALT',        'iSxLXD$OxEn{45T3&PX}VH[JSt~p;OjlqhnWj)7[g~VKU.h:W6QXPq3g07h[VDTd');
define('SECURE_AUTH_SALT', '(J=|<-L1nz)t>ZR71zVqZ5+B8#d1WW*$96F;JqTPj/6Yl#(9=`2mN&ZGMeL(ucjD');
define('LOGGED_IN_SALT',   '%RA~lf3tdW]]V~icy^+;jTF]z<el~mqcHd?G>SvvxtG*iN@8D-2]0+w5!4p?b@h`');
define('NONCE_SALT',       'X u9hjsgzr0l=*-ea%u|L,4v&B0=X^U!Y;M<Tb*c[VnaIL{@13`GK[+}%ePA{lB3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ws_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
